#!/usr/bin/bash

# Целевая директория, где будут храниться файлы страницы.
PATHNAME=.Startpage

mkdir ~/$PATHNAME

# Скрипт для открытия нашей страницы при открытии новой вкладки в ФФ,
#	Здесь правится путь.
cat startPage.uc.js | sed -e "s/your\/directory/\/home\/${USER}\/${PATHNAME}\/page.html/" > chrome/startPage.uc.js

mv -t ~/$PATHNAME page.html script.js jquery.js 2.gif background.png
#	Получение каталога от профиля ФФ.
PROFILEM=$(cat ~/.mozilla/firefox/profiles.ini | sed -n '/^Default/p' | head -n1 | awk -F "=" '{print $2}')
mv chrome ~/.mozilla/firefox/$PROFILEM